package contact

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

type ContactSection struct {
	Contacts []contactEntry
}

func emptyContactSection() ContactSection {
	return ContactSection{}
}

func newContactSection(contacts []contactEntry) ContactSection {
	return ContactSection{Contacts: contacts}
}

type contactEntry struct {
	Name        string
	ContactType string
	Value       string
}

func emptyContactEntry() contactEntry {
	return contactEntry{}
}

func newContactEntry(name, contactType, value string) contactEntry {
	return contactEntry{
		Name:        name,
		ContactType: contactType,
		Value:       value,
	}
}

func GetContactSection(path string) (ContactSection, error) {
	input, err := os.ReadFile(path)
	if err != nil {
		return emptyContactSection(), err
	}
	return parseContactSection(input)
}

func parseContactSection(input []byte) (ContactSection, error) {
	model := emptyContactSection()

	err := json.Unmarshal(input, &model)
	if err != nil {
		log.Println(err)
		return emptyContactSection(), err
	}

	return model, nil
}

func (c ContactSection) ConvertContactSectionIntoMarkdown() string {
	text := "## Contact\n\n"
	for _, contact := range c.Contacts {
		switch contact.ContactType {
		case "url":
			text = text + fmt.Sprintf("- Website: [%s](%s) \n", contact.Value, contact.Value)
		case "mail":
			text = text + fmt.Sprintf("- E-mail: [%s](mailto:%s) \n", contact.Value, contact.Value)
		case "phone":
			text = text + fmt.Sprintf("- Phone: %s \n", contact.Value)
		default:
			text = text + fmt.Sprintf("- %s: %s \n", contact.ContactType, contact.Value)
		}
	}

	return text
}
