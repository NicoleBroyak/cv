package main

import (
	"log"

	"gitlab.com/nicolebroyak/cv/resume"
)

func main() {
	resume := resume.EmptyResume()
	err := resume.EnrichEmptyResume()
	if err != nil {
		log.Fatalf("Enrich resume error: %v", err)
	}

	err = resume.ParseResumeIntoMarkdownFile("Nikola Broyak", "./README.md")
	if err != nil {
		log.Fatalf("Parse resume into .md error: %v", err)
	}
}
