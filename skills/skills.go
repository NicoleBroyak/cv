package skills

import (
	"bufio"
	"bytes"
	"os"
)

func GetSkills(path string) ([]string, error) {
	return getFileLines(path)
}

func getFileLines(path string) ([]string, error) {
	input, err := os.ReadFile(path)
	if err != nil {
		return []string{}, err
	}

	reader := bytes.NewReader(input)
	scanner := bufio.NewScanner(reader)
	lines := make([]string, 0)

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	return lines, nil
}
