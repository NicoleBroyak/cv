module gitlab.com/nicolebroyak/cv

go 1.19

require github.com/gobuffalo/nulls v0.4.2

replace gitlab.com/nicolebroyak/cv/skills v0.0.1 => ./skills

replace gitlab.com/nicolebroyak/cv/education v0.0.1 => ./education

replace gitlab.com/nicolebroyak/cv/employment v0.0.1 => ./employment

replace gitlab.com/nicolebroyak/cv/about v0.0.1 => ./about

replace gitlab.com/nicolebroyak/cv/resume v0.0.1 => ./resume

require github.com/gofrs/uuid v4.2.0+incompatible // indirect
