package education

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/gobuffalo/nulls"
)

type facilityType string

const (
	secondarySchool     = facilityType("Secondary School")
	bootcamp            = facilityType("Bootcamp")
	certificationCentre = facilityType("Certification centre")
	university          = facilityType("University")
	vocationalSchool    = facilityType("Vocational school")
)

type EducationHistory struct {
	Education []educationEntry
}

func emptyEducationHistory() EducationHistory {
	return EducationHistory{}
}

func newEducationHistory(education []educationEntry) EducationHistory {
	return EducationHistory{Education: education}
}

func GetEducationHistory(path string) (EducationHistory, error) {
	input, err := os.ReadFile(path)
	if err != nil {
		return emptyEducationHistory(), err
	}
	return parseEducationHistory(input)
}

func parseEducationHistory(input []byte) (EducationHistory, error) {
	model := emptyEducationHistory()

	err := json.Unmarshal(input, &model)
	if err != nil {
		log.Println(err)
		return emptyEducationHistory(), err
	}

	return model, nil
}

func (e EducationHistory) ConvertEducationHistoryIntoMarkdown() string {
	header := "## Education & Certification \n\n"

	var text string
	for _, entry := range e.Education {
		var educationMD string
		educationMD = fmt.Sprintf("\n#### %s `%s`\n\n", entry.CertificationTitle, entry.FacilityType)
		if entry.DateEnd.Valid {
			educationMD = educationMD + fmt.Sprintf("- Date: `%s - %s`\n", entry.DateStart, entry.DateEnd.String)
		} else {
			educationMD = educationMD + fmt.Sprintf("- Date: `%s - now`\n", entry.DateStart)
		}

		educationMD = educationMD + fmt.Sprintf("- Facility name: `%s`\n", entry.FacilityName)

		educationMD = educationMD + "\n"
		text = educationMD + text
	}

	return header + text
}

type educationEntry struct {
	DateStart          string
	DateEnd            nulls.String
	FacilityType       facilityType
	FacilityName       string
	CertificationTitle string
}

// every struct deserves constructors :)
func newEducationEntry(
	dateStart string,
	dateEnd nulls.String,
	facilityType facilityType,
	facilityName string,
	certificationTitle string,
) educationEntry {
	return educationEntry{
		DateStart:          dateStart,
		DateEnd:            dateEnd,
		FacilityType:       facilityType,
		FacilityName:       facilityName,
		CertificationTitle: certificationTitle,
	}
}

func emptyEducationEntry() educationEntry {
	return educationEntry{}
}
