package employment

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/gobuffalo/nulls"
)

type EmploymentHistory struct {
	Employment []jobEntry
}

func emptyEmploymentHistory() EmploymentHistory {
	return EmploymentHistory{}
}

func newEmploymentHistory(employment []jobEntry) EmploymentHistory {
	return EmploymentHistory{Employment: employment}
}

func GetEmploymentHistory(path string) (EmploymentHistory, error) {
	input, err := os.ReadFile(path)
	if err != nil {
		return emptyEmploymentHistory(), err
	}
	return parseEmploymentHistory(input)
}

func parseEmploymentHistory(input []byte) (EmploymentHistory, error) {
	model := emptyEmploymentHistory()

	err := json.Unmarshal(input, &model)
	if err != nil {
		log.Println(err)
		return emptyEmploymentHistory(), err
	}

	return model, nil
}

func (e EmploymentHistory) ConvertEmploymentHistoryIntoMarkdown() string {
	header := "## Employment History\n\n"

	var text string
	for _, job := range e.Employment {
		var jobEntry string
		if job.Company.Valid {
			jobEntry = fmt.Sprintf("\n#### %s `%s`\n\n", job.Position, job.Company.String)
		} else {
			jobEntry = fmt.Sprintf("\n#### %s\n\n", job.Position)
		}

		if job.DateEnd.Valid {
			jobEntry = jobEntry + fmt.Sprintf("- Date: `%s - %s`\n", job.DateStart, job.DateEnd.String)
		} else {
			jobEntry = jobEntry + fmt.Sprintf("- Date: `%s - now`\n", job.DateStart)
		}

		if job.Description.Valid {
			jobEntry = jobEntry + fmt.Sprintf("- Description: `%s`\n", job.Description.String)
		}
		if len(job.Responsibilities) > 0 {
			jobEntry = jobEntry + "- Responsibilities:\n"
			for _, responsibility := range job.Responsibilities {
				jobEntry = jobEntry + fmt.Sprintf("	- %s\n", responsibility)
			}
		}

		jobEntry = jobEntry + "\n---\n"
		text = jobEntry + text
	}

	return header + text + "\n"
}

type jobEntry struct {
	DateStart        string
	DateEnd          nulls.String
	Company          nulls.String
	Position         string
	Description      nulls.String
	Responsibilities []string
}

// every struct deserves constructors :)
func NewJobEntry(
	dateStart string,
	dateEnd nulls.String,
	company nulls.String,
	position string,
	description nulls.String,
	responsibilities []string,
) jobEntry {
	return jobEntry{
		DateStart:        dateStart,
		DateEnd:          dateEnd,
		Company:          company,
		Position:         position,
		Description:      description,
		Responsibilities: responsibilities,
	}
}

func emptyJobEntry() jobEntry {
	return jobEntry{}
}
