package resume

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/nicolebroyak/cv/about"
	"gitlab.com/nicolebroyak/cv/contact"
	"gitlab.com/nicolebroyak/cv/education"
	"gitlab.com/nicolebroyak/cv/employment"
	"gitlab.com/nicolebroyak/cv/skills"
)

type Resume struct {
	AboutMe    about.AboutMeSection
	Contact    contact.ContactSection
	Education  education.EducationHistory
	Employment employment.EmploymentHistory
	SoftSkills []string
	HardSkills []string
}

func EmptyResume() Resume {
	return Resume{}
}

func NewResume(
	aboutMe about.AboutMeSection,
	contact contact.ContactSection,
	education education.EducationHistory,
	employment employment.EmploymentHistory,
	softSkills []string,
	hardSkills []string,
) Resume {
	return Resume{
		AboutMe:    aboutMe,
		Contact:    contact,
		Education:  education,
		Employment: employment,
		SoftSkills: softSkills,
		HardSkills: hardSkills,
	}
}

func (r *Resume) EnrichEmptyResume() error {
	var err error
	r.SoftSkills, err = skills.GetSkills("./skills/soft_skills.txt")
	if err != nil {
		return err
	}

	r.HardSkills, err = skills.GetSkills("./skills/hard_skills.txt")
	if err != nil {
		return err
	}

	r.Employment, err = employment.GetEmploymentHistory("./employment/employment.json")
	if err != nil {
		return err
	}

	r.Education, err = education.GetEducationHistory("./education/education.json")
	if err != nil {
		return err
	}

	r.AboutMe, err = about.GetAboutMeSection("./about/about_me.json")
	if err != nil {
		return err
	}

	r.Contact, err = contact.GetContactSection("./contact/contact.json")
	if err != nil {
		return err
	}

	return nil
}

func (r Resume) createFooter() (string, error) {
	text := fmt.Sprintf("\n---\n## Footer\n\n*resume generated at %v UTC by __gitlab.com/nicolebroyak/cv__ app*\n\n", time.Now().UTC().Format("02-01-2006 15:04:05"))
	footer, err := os.ReadFile("./resume/footer.txt")
	if err != nil {
		return "", err
	}

	text += string(footer)
	return text, nil
}

func (r Resume) ParseResumeIntoMarkdownFile(name string, outputPath string) error {
	text := fmt.Sprintf("# %s - Resume\n\n", name)
	text += r.AboutMe.ConvertAboutMeIntoMarkdown()
	text += r.ConvertSkillsIntoMarkdown()
	text += r.Employment.ConvertEmploymentHistoryIntoMarkdown()
	text += r.Education.ConvertEducationHistoryIntoMarkdown()
	text += r.Contact.ConvertContactSectionIntoMarkdown()
	footer, err := r.createFooter()
	if err != nil {
		return err
	}

	text += footer
	return os.WriteFile(outputPath, []byte(text), 0777)
}

func (r Resume) ConvertSkillsIntoMarkdown() string {
	text := "## Hard & Soft Skills\n" +
		"| Hard Skills | Soft Skills |\n" +
		"| :---: | --- |\n"

	softSkillsLen := len(r.SoftSkills)
	hardSkillsLen := len(r.HardSkills)
	var maxLen int
	if softSkillsLen > hardSkillsLen {
		maxLen = softSkillsLen
	} else {
		maxLen = hardSkillsLen
	}

	var row string
	for i := 0; i < maxLen; i++ {
		if i < hardSkillsLen {
			row = row + fmt.Sprintf("| `%s` |", r.HardSkills[i])
		} else {
			row = row + "| |"
		}
		if i < softSkillsLen {
			row = row + fmt.Sprintf(" %s |\n", r.SoftSkills[i])
		} else if i != maxLen-1 {
			row = row + " |\n"
		} else {
			row = row + " ||\n"
		}
	}

	text = text + row + "\n\n"

	return text
}
