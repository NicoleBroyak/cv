package about

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/gobuffalo/nulls"
)

type AboutMeSection struct {
	Titles         []title
	AdditionalInfo []string
}

func emptyAboutMeSection() AboutMeSection {
	return AboutMeSection{}
}

func newAboutMeSection(titles []title, additionalInfo []string) AboutMeSection {
	return AboutMeSection{
		Titles:         titles,
		AdditionalInfo: additionalInfo,
	}
}

func GetAboutMeSection(path string) (AboutMeSection, error) {
	input, err := os.ReadFile(path)
	if err != nil {
		return emptyAboutMeSection(), err
	}
	return parseAboutMeSection(input)
}

func parseAboutMeSection(input []byte) (AboutMeSection, error) {
	model := emptyAboutMeSection()

	err := json.Unmarshal(input, &model)
	if err != nil {
		log.Println(err)
		return emptyAboutMeSection(), err
	}

	return model, nil
}

type title struct {
	Name        string
	Description nulls.String
}

func emptyTitle() title {
	return title{}
}

func newTitle(name string, description nulls.String) title {
	return title{Name: name, Description: description}
}

func (a AboutMeSection) ConvertAboutMeIntoMarkdown() string {
	text := "## About Me\n\n"
	for _, title := range a.Titles {
		if title.Description.Valid {
			text = text + fmt.Sprintf("- **%s** - %s\n", title.Name, title.Description.String)
		} else {
			text = text + fmt.Sprintf("- **%s**\n", title.Name)
		}
	}
	text = text + "\n"
	for _, info := range a.AdditionalInfo {
		text = text + fmt.Sprintf("%s\n", info)
	}
	text = text + "\n\n"

	return text
}
