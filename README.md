# Nikola Broyak - Resume

## About Me

- **Former Junior Golang Developer** - Websensa
- **Snooker Player** - Snooker.Zone Warsaw
- **Industrious Person** - Wherever it is needed

I am willing to work in an environment which can benefit from my personality and abilities.
My life-engine is powered by the need of constant self-development and sense of purpose.


## Hard & Soft Skills
| Hard Skills | Soft Skills |
| :---: | --- |
| `office (doc/spreadsheet) software` | attention to detail |
| `golang` | assertiveness |
| `grpc` | self-reliance |
| `redis` | empathy |
| `mysql` | ability to learn new technologies |
| `clickhouse` | determination in improvement of current skills |
| `mongodb` | ability to ask for help when advisable  |
| `git` | ability to follow and learn from work superiors |
| `linux` | |
| `zsh` | |
| `vim` | |
| `jira` | |
| `confluence` | |
| `python` | |
| `django` | |
| `docker` | ||


## Employment History


#### Intern + Junior Go/Python Developer `WEBSENSA Co.`

- Date: `03.2022 - 12.2022`
- Description: `Working on two backend components of large scale environment`
- Responsibilities:
	- learning codebase and its purpose
	- maintenance of the projects
	- migrating Python functionalities to Golang
	- deploying app on dedicated CI-helping utility
	- implementing cronjob apps related to projects

---

#### Senior Customer Care Specialist `STS S.A.`

- Date: `07.2017 - 01.2022`
- Description: `Running betting shop on my own`
- Responsibilities:
	- taking care of customers
	- keeping financial records and other office responsibilities

---

#### Odd jobs

- Date: `07.2016 - 06.2017`

---

#### Accountant `Angels of Enterpreneurship Co.`

- Date: `01.2016 - 06.2016`
- Description: `Partially substituting Chief Accountant during her leave`
- Responsibilities:
	- validation of junior accountants' assignments
	- calculating and writing yearly financial statements of companies
	- implementing booking automation in Comarch ERP Optima software
	- contact with clients and government officials

---

#### Junior Accountant `Angels of Enterpreneurship Co.`

- Date: `09.2015 - 12.2015`
- Responsibilities:
	- doing current booking assignments
	- preparation of monthly and yearly tax declarations

---

#### Intern `Angels of Enterpreneurship Co.`

- Date: `08.2015 - 08.2015`
- Responsibilities:
	- doing current booking assignments
	- preparation of monthly and yearly tax declarations

---

#### Intern `Angels of Enterpreneurship Co.`

- Date: `02.2015 - 02.2015`
- Responsibilities:
	- doing current booking assignments
	- preparation of monthly and yearly tax declarations

---

## Education & Certification 


#### Python Developer `Bootcamp`

- Date: `07-2021 - 10-2021`
- Facility name: `Future Collars`


#### Accounting Technician `Certification centre`

- Date: `09-2015 - 05-2016`
- Facility name: `Continuing Professional Development Center #1 in Warsaw`


#### Economics Technician `Secondary school`

- Date: `09-2012 - 06-2016`
- Facility name: `Secondary Technical School Of Economics #8 in Warsaw`

## Contact

- E-mail: [nikola@geecko.la](mailto:nikola@geecko.la) 
- Phone: +48 780 113 350 
- Website: [https://linkedin.com/in/nikola-broyak/](https://linkedin.com/in/nikola-broyak/) 
- Website: [https://gitlab.com/nicolebroyak](https://gitlab.com/nicolebroyak) 
- Website: [https://github.com/nicolebroyak](https://github.com/nicolebroyak) 
- Website: [https://geecko.la](https://geecko.la) 

---
## Footer

*resume generated at 23-02-2023 18:38:50 UTC by __gitlab.com/nicolebroyak/cv__ app*

I hereby give consent for my personal data to be processed for the purpose of conducting recruitment for the position for which I am applying.
I also consent to processing of my personal data for the purposes of any future recruitment processes.
I agree to the processing of personal data provided in this document for realising the recruitment process 
pursuant to the Personal Data Protection Act of 10 May 2018 (Journal of Laws 2018, item 1000) 
and in agreement with Regulation (EU) 2016/679 of the European Parliament and of the Council of 27 April
2016 on the protection of natural Persons with regard to the processing of personal data and on the free
movement of such data, and repealing directive 95/46/EC (General Data Protection Regulation)
